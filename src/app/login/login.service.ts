import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

var config = require('../app.config.js');

@Injectable()
export class LoginService {

    constructor(private http: Http) {

    }


    login(email: string, password: string) {

        var url = config.api + 'login';
        console.log(url);

        return this.http.post(url, { email: email, password: password }).toPromise();

    }
}