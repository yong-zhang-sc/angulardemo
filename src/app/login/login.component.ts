import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { LoginService } from './login.service';
import { Router } from '@angular/router';


@Component({
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
    providers: [LoginService]
})
export class LoginComponent {

    sys_name: string = 'Demo Shopping System';

    email: string = '';
    password: string = '';

    constructor(private toastr: ToastrService, private loginService: LoginService, private router: Router) {

    }

    onSubmit(f: NgForm) {
        console.log(f);

        if (f.controls.email.invalid) {
            if (f.controls.email.errors.required) {
                this.toastr.warning('Please input your email address.');
                return false;
            }
        }

        if (f.controls.password.invalid) {
            if (f.controls.password.errors.required) {
                this.toastr.warning('Please input your password.');
                return false;
            }
        }

        this.loginService.login(this.email, this.password)
            .then(response => {
                // console.log(response);
                this.toastr.success('Login success!');
                window.sessionStorage.setItem('user', this.email);

                this.router.navigate(['home']);
            })
            .catch(error => console.log(error));

        return false;
    }

    reset() {
        this.email = '';
        this.password = '';
    }
}