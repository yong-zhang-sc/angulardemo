export class ItemDto {
    Id: number;
    Name: string;
    Price: number;
    Image: string;
}