import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { routing } from './app.routing';
import { FormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { HttpModule } from '@angular/http';
import { ModalModule } from 'ngx-bootstrap/modal';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './home/header.component';
import { SidebarComponent } from './home/sidebar.component';
import { CategoryComponent } from './category/category.component';
import { TodayDealComponent } from './today/today.component';
import { CartComponent } from './cart/cart.component';
import { SwitchUserComponent } from './home/switchuser.component';

@NgModule({
  imports: [
    BrowserModule,
    routing,
    FormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    HttpModule,
    ModalModule.forRoot()
  ],
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    HeaderComponent,
    SidebarComponent,
    CategoryComponent,
    TodayDealComponent,
    CartComponent,
    SwitchUserComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
