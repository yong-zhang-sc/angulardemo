import { Component, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
    selector: 'demo-switchuser',
    templateUrl: './switchuser.component.html'
})
export class SwitchUserComponent {

    @Input() currentUser: string = '';
    email: string = '';
    password: string = '';

    @Output() onUserSwitched = new EventEmitter<string>()

    @ViewChild('childModal') public childModal: ModalDirective;

    public showChildModal(): void {
        this.email = '';
        this.password = '';
        this.childModal.show();
    }

    public hideChildModal(): void {
        this.childModal.hide();
    }

    switchuser() {
        this.onUserSwitched.next(this.email);
        this.childModal.hide();
    }


}