import { Component } from '@angular/core';

@Component({
    selector: 'demo-header',
    templateUrl: './header.component.html'
})
export class HeaderComponent {

    user: string = '';

    ngOnInit() {
        this.user = window.sessionStorage.getItem('user');
    }

    switchToUser(email: string) {
        this.user = email;
        window.sessionStorage.setItem('user', email);
    }
}