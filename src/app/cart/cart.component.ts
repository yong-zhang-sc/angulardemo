import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { CartService } from './cart.service';
import { Subscription } from 'rxjs';
import { ItemDto } from '../dto/Item.dto';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
    selector: 'demo-cart',
    templateUrl: './cart.component.html',
    styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit, OnDestroy {

    private subscription: Subscription;
    private items: Map<ItemDto, number> = new Map<ItemDto, number>();

    view_items: Array<any> = new Array<any>();

    @ViewChild('childModal') public childModal: ModalDirective;

    constructor(private cartService: CartService) {

    }

    ngOnInit() {
        this.subscription = this.cartService.onNewItemAddedEvent.subscribe(item => {

            if (this.items.has(item)) {
                this.items.set(item, this.items.get(item) + 1);
            } else {
                this.items.set(item, 1);
            }

            this.view_items = new Array<any>();
            var iterator = this.items.entries();

            for (let entry of iterator) {

                console.log(entry);

                this.view_items.push({
                    item: entry[0],
                    count: entry[1]
                });
            }
        });
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }



    public showChildModal(): void {
        this.childModal.show();
    }

    public hideChildModal(): void {
        this.childModal.hide();
    }
}