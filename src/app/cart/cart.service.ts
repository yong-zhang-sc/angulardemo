import { Injectable } from '@angular/core';
import { ItemDto } from '../dto/Item.dto';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class CartService {

    private addNewItemToCardSource = new Subject<ItemDto>();
    public onNewItemAddedEvent = this.addNewItemToCardSource.asObservable();

    public addToCart(item: ItemDto) {
        this.addNewItemToCardSource.next(item);
    }

}