import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CategoryService } from './category.service';
import { ItemDto } from '../dto/Item.dto';
import { CartService } from '../cart/cart.service';

@Component({
    templateUrl: './category.component.html',
    providers: [CategoryService]
})
export class CategoryComponent {

    items: Array<ItemDto> = [];

    constructor(private route: ActivatedRoute, private categoryService: CategoryService, private cartService: CartService) {
        route.params.map(p => p.cid).subscribe(cid => {

            this.categoryService.getItemByCategory(cid)
                .then(data => this.items = data);

        });
    }

    addToCart(item: ItemDto) {
        this.cartService.addToCart(item);
    }
}