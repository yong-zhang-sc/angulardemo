import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { ItemDto } from '../dto/Item.dto';

var config = require('../app.config.js');

@Injectable()
export class CategoryService {

    constructor(private http: Http) {
    }

    getItemByCategory(cid: number): Promise<Array<ItemDto>> {

        var url = config.api + 'category/' + cid;

        return this.http.get(url).map(response => response.json()).toPromise();
    }
}